import string
import random


def generate_random_user_id(length=8):
    """
    generate random user id
    """

    letters = string.ascii_lowercase
    rnd_str = ''.join(random.choice(letters) for i in range(length-3))
    rnd_num = random.randint(100, 999)

    return "user_" + rnd_str + str(rnd_num)

