from __future__ import division

from redis import StrictRedis, Redis, ConnectionPool
import math

from itertools import zip_longest


class Leaderboard(object):

    DEFAULT_PAGE_SIZE = 25
    # DEFAULT_REDIS_HOST = 'localhost'
    DEFAULT_REDIS_HOST = 'redis'
    DEFAULT_REDIS_PORT = 6379
    DEFAULT_REDIS_DB = 0
    DEFAULT_MEMBER_DATA_NAMESPACE = 'member_data'
    DEFAULT_GLOBAL_MEMBER_DATA = False
    DEFAULT_POOLS = {}
    ASC = 'asc'
    DESC = 'desc'
    MEMBER_KEY = 'member'
    MEMBER_DATA_KEY = 'member_data'
    SCORE_KEY = 'score'
    RANK_KEY = 'rank'

    @classmethod
    def pool(self, host, port, db, pools={}, **options):
        """
        Fetch a redis connection pool
        """
        key = (host, port, db)
        rval = pools.get(key)
        if not isinstance(rval, ConnectionPool):
            rval = ConnectionPool(host=host, port=port, db=db, **options)
            pools[key] = rval

        return rval

    def __init__(self, leaderboard_name, **options):
        """
        Initialize a connection to a specific leaderboard. By default, will use a
        redis connection pool for any unique host:port:db pairing.
        """
        self.leaderboard_name = leaderboard_name
        self.options = options

        self.page_size = self.options.pop('page_size', self.DEFAULT_PAGE_SIZE)
        if self.page_size < 1:
            self.page_size = self.DEFAULT_PAGE_SIZE

        self.member_data_namespace = self.options.pop(
            'member_data_namespace',
            self.DEFAULT_MEMBER_DATA_NAMESPACE)
        self.global_member_data = self.options.pop(
            'global_member_data',
            self.DEFAULT_GLOBAL_MEMBER_DATA)

        self.order = self.options.pop('order', self.DESC).lower()
        if not self.order in [self.ASC, self.DESC]:
            raise ValueError(
                "%s is not one of [%s]" % (self.order, ",".join([self.ASC, self.DESC])))

        redis_connection = self.options.pop('redis_connection', None)
        if redis_connection:
            # allow the developer to pass a raw redis connection and
            # we will use it directly instead of creating a new one
            self.redis_connection = redis_connection
        else:
            connection = self.options.pop('connection', None)
            if isinstance(connection, (StrictRedis, Redis)):
                self.options['connection_pool'] = connection.connection_pool
            if 'connection_pool' not in self.options:
                self.options['connection_pool'] = self.pool(
                    self.options.pop('host', self.DEFAULT_REDIS_HOST),
                    self.options.pop('port', self.DEFAULT_REDIS_PORT),
                    self.options.pop('db', self.DEFAULT_REDIS_DB),
                    self.options.pop('pools', self.DEFAULT_POOLS),
                    **self.options
                )
            self.redis_connection = Redis(**self.options)

    def rank_member(self, member, score, member_data=None):
        """
        Rank a member in the leaderboard.

        @param member [String] Member name.
        @param score [float] Member score.
        @param member_data [String] Optional member data.
        """
        self.rank_member_in(self.leaderboard_name, member, score, member_data)

    def rank_member_in(
            self, leaderboard_name, member, score, member_data=None):
        """
        Rank a member in the named leaderboard.
        """
        pipeline = self.redis_connection.pipeline()
        if isinstance(self.redis_connection, Redis):
            pipeline.zadd(leaderboard_name, member, score)
        else:
            pipeline.zadd(leaderboard_name, score, member)
        if member_data:
            pipeline.hset(
                self._member_data_key(leaderboard_name),
                member,
                member_data)
        pipeline.execute()

    def rank_member_across(
            self, leaderboards, member, score, member_data=None):
        """
        Rank a member across multiple leaderboards.
        """
        pipeline = self.redis_connection.pipeline()
        for leaderboard_name in leaderboards:
            if isinstance(self.redis_connection, Redis):
                pipeline.zadd(leaderboard_name, member, score)
            else:
                pipeline.zadd(leaderboard_name, score, member)
            if member_data:
                pipeline.hset(
                    self._member_data_key(leaderboard_name),
                    member,
                    member_data)
        pipeline.execute()

    def total_members(self):
        """
        Retrieve the total number of members in the leaderboard.
        """
        return self.total_members_in(self.leaderboard_name)

    def total_members_in(self, leaderboard_name):
        """
        Retrieve the total number of members in the named leaderboard.
        """
        return self.redis_connection.zcard(leaderboard_name)

    def remove_member(self, member):
        """
        Remove a member from the leaderboard.
        """
        self.remove_member_from(self.leaderboard_name, member)

    def remove_member_from(self, leaderboard_name, member):
        """
        Remove the optional member data for a given member in the named leaderboard.
        """
        pipeline = self.redis_connection.pipeline()
        pipeline.zrem(leaderboard_name, member)
        pipeline.hdel(self._member_data_key(leaderboard_name), member)
        pipeline.execute()

    def total_pages(self, page_size=None):
        """
        Retrieve the total number of pages in the leaderboard.
        """
        return self.total_pages_in(self.leaderboard_name, page_size)

    def total_pages_in(self, leaderboard_name, page_size=None):
        """
        Retrieve the total number of pages in the named leaderboard.
        """
        if page_size is None:
            page_size = self.page_size

        return int(
            math.ceil(
                self.total_members_in(leaderboard_name) /
                float(page_size)))

    def total_members_in_score_range(self, min_score, max_score):
        """
        Retrieve the total members in a given score range from the leaderboard.
        """
        return self.total_members_in_score_range_in(
            self.leaderboard_name, min_score, max_score)

    def total_members_in_score_range_in(
            self, leaderboard_name, min_score, max_score):
        """
        Retrieve the total members in a given score range from the named leaderboard.
        """
        return self.redis_connection.zcount(
            leaderboard_name, min_score, max_score)

    def total_scores(self):
        """
        Sum of scores for all members in the leaderboard.
        """
        return self.total_scores_in(self.leaderboard_name)

    def total_scores_in(self, leaderboard_name):
        """
        Sum of scores for all members in the named leaderboard.
        """
        return sum([leader[self.SCORE_KEY] for leader in self.all_leaders_from(self.leaderboard_name)])

    def check_member(self, member):
        """
        Check to see if a member exists in the leaderboard.
        """
        return self.check_member_in(self.leaderboard_name, member)

    def check_member_in(self, leaderboard_name, member):
        """
        Check to see if a member exists in the named leaderboard.
        """
        return self.redis_connection.zscore(
            leaderboard_name, member) is not None

    def rank_for(self, member):
        """
        Retrieve the rank for a member in the leaderboard.
        """
        return self.rank_for_in(self.leaderboard_name, member)

    def rank_for_in(self, leaderboard_name, member):
        """
        Retrieve the rank for a member in the named leaderboard.
        """
        if self.order == self.ASC:
            try:
                return self.redis_connection.zrank(
                    leaderboard_name, member) + 1
            except:
                return None
        else:
            try:
                return self.redis_connection.zrevrank(
                    leaderboard_name, member) + 1
            except:
                return None

    def score_for(self, member):
        """
        Retrieve the score for a member in the leaderboard.
        """
        return self.score_for_in(self.leaderboard_name, member)

    def score_for_in(self, leaderboard_name, member):
        """
        Retrieve the score for a member in the named leaderboard.
        """
        score = self.redis_connection.zscore(leaderboard_name, member)
        if score is not None:
            score = float(score)

        return score

    def score_and_rank_for(self, member):
        """
        Retrieve the score and rank for a member in the leaderboard.
        """
        return self.score_and_rank_for_in(self.leaderboard_name, member)

    def score_and_rank_for_in(self, leaderboard_name, member):
        """
        Retrieve the score and rank for a member in the named leaderboard.
        """
        return {
            self.MEMBER_KEY: member,
            self.SCORE_KEY: self.score_for_in(leaderboard_name, member),
            self.RANK_KEY: self.rank_for_in(leaderboard_name, member)
        }

    def change_score_for(self, member, delta, member_data=None):
        """
        Change the score for a member in the leaderboard by a score delta which can be positive or negative.
        """
        self.change_score_for_member_in(self.leaderboard_name, member, delta, member_data)

    def change_score_for_member_in(self, leaderboard_name, member, delta, member_data=None):
        """
        Change the score for a member in the named leaderboard by a delta which can be positive or negative.
        """
        pipeline = self.redis_connection.pipeline()
        pipeline.zincrby(leaderboard_name, member, delta)
        if member_data:
            pipeline.hset(
                self._member_data_key(leaderboard_name),
                member,
                member_data)
        pipeline.execute()

    def remove_members_in_score_range(self, min_score, max_score):
        """
        Remove members from the leaderboard in a given score range.
        """
        self.remove_members_in_score_range_in(
            self.leaderboard_name,
            min_score,
            max_score)

    def remove_members_in_score_range_in(
            self, leaderboard_name, min_score, max_score):
        """
        Remove members from the named leaderboard in a given score range.
        """
        self.redis_connection.zremrangebyscore(
            leaderboard_name,
            min_score,
            max_score)

    def remove_members_outside_rank(self, rank):
        """
        Remove members from the leaderboard in a given rank range.
        """
        return self.remove_members_outside_rank_in(self.leaderboard_name, rank)

    def remove_members_outside_rank_in(self, leaderboard_name, rank):
        """
        Remove members from the named leaderboard in a given rank range.
        """
        if self.order == self.DESC:
            rank = -(rank) - 1
            return self.redis_connection.zremrangebyrank(
                leaderboard_name, 0, rank)
        else:
            return self.redis_connection.zremrangebyrank(
                leaderboard_name, rank, -1)

    def members_from_rank_range(self, starting_rank, ending_rank, **options):
        """
        Retrieve members from the leaderboard within a given rank range.
        """
        return self.members_from_rank_range_in(
            self.leaderboard_name, starting_rank, ending_rank, **options)

    def members_from_rank_range_in(
            self, leaderboard_name, starting_rank, ending_rank, **options):
        """
        Retrieve members from the named leaderboard within a given rank range.
        """
        starting_rank -= 1
        if starting_rank < 0:
            starting_rank = 0

        ending_rank -= 1
        if ending_rank > self.total_members_in(leaderboard_name):
            ending_rank = self.total_members_in(leaderboard_name) - 1

        raw_leader_data = []
        if self.order == self.DESC:
            raw_leader_data = self.redis_connection.zrevrange(
                leaderboard_name,
                starting_rank,
                ending_rank,
                withscores=False)
        else:
            raw_leader_data = self.redis_connection.zrange(
                leaderboard_name,
                starting_rank,
                ending_rank,
                withscores=False)

        return self._parse_raw_members(
            leaderboard_name, raw_leader_data, **options)

    def top(self, number, **options):
        """
        Retrieve members from the leaderboard within a range from 1 to the number given.
        """
        return self.top_in(self.leaderboard_name, number, **options)

    def top_in(self, leaderboard_name, number, **options):
        """
        Retrieve members from the named leaderboard within a range from 1 to the number given.
        """
        return self.members_from_rank_range_in(leaderboard_name, 1, number, **options)

    def member_at(self, position, **options):
        """
        Retrieve a member at the specified index from the leaderboard.
        """
        return self.member_at_in(self.leaderboard_name, position, **options)

    def member_at_in(self, leaderboard_name, position, **options):
        """
        Retrieve a member at the specified index from the leaderboard.
        """
        if position > 0 and position <= self.total_members_in(leaderboard_name):
            page_size = options.get('page_size', self.page_size)
            current_page = math.ceil(float(position) / float(page_size))
            offset = (position - 1) % page_size

            leaders = self.leaders_in(
                leaderboard_name,
                current_page,
                **options)
            if leaders:
                return leaders[offset]

    def member_around_me(self, member, **options):
        """
        Retrieve a page of leaders from the leaderboard around a given member.
        """
        return self.around_me_in(self.leaderboard_name, member, **options)

    def around_me_in(self, leaderboard_name, member, **options):
        """
        Retrieve a page of leaders from the named leaderboard around a given member.
        """
        reverse_rank_for_member = None
        if self.order == self.DESC:
            reverse_rank_for_member = self.redis_connection.zrevrank(
                leaderboard_name,
                member)
        else:
            reverse_rank_for_member = self.redis_connection.zrank(
                leaderboard_name,
                member)

        if reverse_rank_for_member is None:
            return []

        page_size = options.get('page_size', self.page_size)

        starting_offset = reverse_rank_for_member - (page_size // 2)
        if starting_offset < 0:
            starting_offset = 0

        ending_offset = (starting_offset + page_size) - 1

        raw_leader_data = self._range_method(
            self.redis_connection,
            leaderboard_name,
            int(starting_offset),
            int(ending_offset),
            withscores=False)
        return self._parse_raw_members(
            leaderboard_name, raw_leader_data, **options)

    def ranked_in_list(self, members, **options):
        """
        Retrieve a page of leaders from the leaderboard for a given list of members.
        """
        return self.ranked_in_list_in(
            self.leaderboard_name, members, **options)

    def ranked_in_list_in(self, leaderboard_name, members, **options):
        """
        Retrieve a page of leaders from the named leaderboard for a given list of members.
        """
        ranks_for_members = []

        pipeline = self.redis_connection.pipeline()

        for member in members:
            if self.order == self.ASC:
                pipeline.zrank(leaderboard_name, member)
            else:
                pipeline.zrevrank(leaderboard_name, member)

            pipeline.zscore(leaderboard_name, member)

        responses = pipeline.execute()

        for index, member in enumerate(members):
            data = {}
            data[self.MEMBER_KEY] = member
            rank = responses[index * 2]
            if rank is not None:
                rank += 1
            else:
                if not options.get('include_missing', True):
                    continue
            data[self.RANK_KEY] = rank
            score = responses[index * 2 + 1]
            if score is not None:
                score = float(score)
            data[self.SCORE_KEY] = score

            ranks_for_members.append(data)

        if ('with_member_data' in options) and (True == options['with_member_data']):
            for index, member_data in enumerate(self.members_data_for_in(leaderboard_name, members)):
                try:
                    ranks_for_members[index][self.MEMBER_DATA_KEY] = member_data
                except:
                    pass

        if 'sort_by' in options:
            sort_value_if_none = float('-inf') if self.order == self.ASC else float('+inf')
            if self.RANK_KEY == options['sort_by']:
                ranks_for_members = sorted(
                    ranks_for_members,
                    key=lambda member: member.get(self.RANK_KEY) if member.get(
                        self.RANK_KEY) is not None else sort_value_if_none
                )
            elif self.SCORE_KEY == options['sort_by']:
                ranks_for_members = sorted(
                    ranks_for_members,
                    key=lambda member: member.get(self.SCORE_KEY) if member.get(
                        self.SCORE_KEY) is not None else sort_value_if_none
                )

        return ranks_for_members

    def _range_method(self, connection, *args, **kwargs):
        if self.order == self.DESC:
            return connection.zrevrange(*args, **kwargs)
        else:
            return connection.zrange(*args, **kwargs)

    def leaders(self, current_page, **options):
        """
        Retrieve a page of leaders from the leaderboard.
        """
        return self.leaders_in(self.leaderboard_name, current_page, **options)

    def leaders_in(self, leaderboard_name, current_page, **options):
        """
        Retrieve a page of leaders from the named leaderboard.
        """
        if current_page < 1:
            current_page = 1

        page_size = options.get('page_size', self.page_size)

        index_for_redis = current_page - 1

        starting_offset = (index_for_redis * page_size)
        if starting_offset < 0:
            starting_offset = 0

        ending_offset = (starting_offset + page_size) - 1

        raw_leader_data = self._range_method(
            self.redis_connection,
            leaderboard_name,
            int(starting_offset),
            int(ending_offset),
            withscores=False)
        return self._parse_raw_members(
            leaderboard_name, raw_leader_data, **options)

    def _parse_raw_members(
            self, leaderboard_name, members, members_only=False, **options):
        """
        Parse the raw leaders data as returned from a given leader board query. Do associative
        lookups with the member to rank, score and potentially sort the results.
        """
        if members_only:
            return [{self.MEMBER_KEY: m} for m in members]

        if members:
            return self.ranked_in_list_in(leaderboard_name, members, **options)
        else:
            return []


def grouper(n, iterable, fillvalue=None):
    args = [iter(iterable)] * n
    return zip_longest(fillvalue=fillvalue, *args)
