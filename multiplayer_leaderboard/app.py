"""Route declaration."""
import random

from flask import Flask
# from flask import current_app as app
from flask import render_template, request, jsonify

from leaderboard import Leaderboard
from utils import generate_random_user_id

app = Flask(__name__, template_folder="templates")

leaderboard = Leaderboard("default")
default_user = 'default_user'

leaderboard.rank_member(default_user, 0)


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/add_score')
def add_score():
    """
    Add score to the user provided and return computed rank and
    total score.
    If no score is provided, default 10 is added.
    """

    try:
        score = int(request.args.get("score"))
        user_id = default_user
        print(score)
        leaderboard.change_score_for(user_id, score)
        sr = leaderboard.score_and_rank_for(default_user)
        response = {}
        # add current user score and rank
        response.update(sr)

        # add total number of users
        total_members = leaderboard.total_members()
        response.update(total_users=total_members)

        # add top scorer and user
        top_scorer = leaderboard.member_at(1)
        response.update(top_score=top_scorer['score'])
        response.update(top_user=top_scorer['member'].decode('utf-8'))

    except Exception as e:
        print(e)
        return jsonify("Value not supported, check value and retry"),415
    return jsonify(response)


@app.route('/reset_score')
def reset_score():
    """
    Add score to the user provided and return computed rank and
    total score.
    If no score is provided, default 10 is added.
    """

    try:
        leaderboard.rank_member(default_user, 0)
        sr = leaderboard.score_and_rank_for(default_user)
        print(sr)


        response = {}

        # add current user score and rank
        response.update(sr)

        # add total number of users
        total_members = leaderboard.total_members()
        response.update(total_users=total_members)

        # add top scorer and user
        top_scorer = leaderboard.member_at(1)
        response.update(top_score=top_scorer['score'])
        response.update(top_user=top_scorer['member'].decode('utf-8'))

    except Exception as e:
        return jsonify("Value not supported, check value and retry"),415

    return jsonify(response)


@app.route('/generate_user')
def generate_user():
    """
    Add score to the user provided and return computed rank and
    total score.
    If no score is provided, default 10 is added.
    """

    try:
        for _ in range(0, 10):
            leaderboard.rank_member(generate_random_user_id(), random.randint(10, 130))
        sr = leaderboard.score_and_rank_for(default_user)

        response = {}

        # add current user score and rank
        response.update(sr)

        # add total number of users
        total_members = leaderboard.total_members()
        response.update(total_users=total_members)

        # add top scorer and user
        top_scorer = leaderboard.member_at(1)
        response.update(top_score=top_scorer['score'])
        response.update(top_user=top_scorer['member'].decode('utf-8'))

    except Exception as e:
        return jsonify("Value not supported, check value and retry"),415

    return jsonify(response)


@app.route('/reset_board')
def reset_board():
    """
    Delete all user and reset board
    """
    leaderboard.remove_members_outside_rank(0)
    leaderboard.rank_member(default_user, 0)

    sr = leaderboard.score_and_rank_for(default_user)

    response = {}

    # add current user score and rank
    response.update(sr)

    # add total number of users
    total_members = leaderboard.total_members()
    response.update(total_users=total_members)

    # add top scorer and user
    top_scorer = leaderboard.member_at(1)
    response.update(top_score=top_scorer['score'])
    response.update(top_user=top_scorer['member'].decode('utf-8'))

    return jsonify(response)


@app.route('/scoreboard')
def scoreboard():

    number = int(request.args.get("number") or 10)
    result = leaderboard.top(number)

    for each in result:
        each['member'] = each['member'].decode('utf-8')

    if not any(each['member'] == default_user for each in result):
        sr = leaderboard.score_and_rank_for(default_user)
        result.append(sr)

    return jsonify(result)


@app.route('/scoreboard_table')
def scoreboard_table():

    number = int(request.args.get("number") or 10)
    result = leaderboard.top(number)

    for each in result:
        print(each)
        each['member'] = each['member'].decode('utf-8')

    if not any(each['member'] == default_user for each in result):
        sr = leaderboard.score_and_rank_for(default_user)
        result.append(sr)
    return render_template('score.html', users=result, value='111')

@app.route('/add_score_user')
def add_score_to_user():
    """
    Add score to the user provided and return computed rank and
    total score.
    If no score is provided, default 10 is added.
    """

    try:
        score = int(request.args.get("score"))
        user_id = request.args.get("user_id")

        if not leaderboard.check_member(user_id):
            return jsonify("no user exists"),401

        print(score)
        leaderboard.change_score_for(user_id, score)
        sr = leaderboard.score_and_rank_for(default_user)
        response = {}
        # add current user score and rank
        response.update(sr)

        # add total number of users
        total_members = leaderboard.total_members()
        response.update(total_users=total_members)

        # add top scorer and user
        top_scorer = leaderboard.member_at(1)
        response.update(top_score=top_scorer['score'])
        response.update(top_user=top_scorer['member'].decode('utf-8'))

    except Exception as e:
        print(e)
        return jsonify("Value not supported, check value and retry"),415
    return jsonify(response)


@app.route('/rank')
def score():
    """
    Add score to the user provided and return computed rank and
    total score.
    If no score is provided, default 10 is added.
    """

    try:
        sr = leaderboard.score_and_rank_for(default_user)
        response = {}
        # add current user score and rank
        response.update(sr)

        # add total number of users
        total_members = leaderboard.total_members()
        response.update(total_users=total_members)

        # add top scorer and user
        top_scorer = leaderboard.member_at(1)
        response.update(top_score=top_scorer['score'])
        response.update(top_user=top_scorer['member'].decode('utf-8'))

    except Exception as e:
        print(e)
        return jsonify("Value not supported, check value and retry"),415
    return jsonify(response)


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)