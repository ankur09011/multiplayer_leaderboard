# Multiplayer Leaderboard

Implementation of multiplayer leader board using Python and Redis

### Installation

Multiplayer Leader can be deployed using `docker-compose`

1. git clone the repo

```
git clone git@gitlab.com:ankur09011/multiplayer_leaderboard.git 
```

2. Run docker-compose up

```
docker-compose up
```

2. Open the browser 

```
http://localhost:5000
```
### LeaderBoard

![](main_d.png)

1. Once the dashboard is initialize there is only 1 player
2. Dashboard display 
    a. Current Score
    b. Current Rank
    c. Total Users
    d. Top Score
    e. Top Rank User

3. Add score
    a. Click on button `Get Coins`, it will add 10 to your score every time it is clicked. Current score and current rank is refreshed every time tghr button is being clicked.
    b. You can add  `n` to current score by providing the value in  text box and clicking add score.  Current score and current rank is refreshed every time tghr button is being clicked.
4. Random user can be generated to check the POC  by clicking the button Generate Random User. This will generate 10 random user every time it is clicked with random score ranging from 10-200.
5. Score can be added to any player by providing user_id amnd score to add in the text at last section
6. Scoreboard can be updated by clicking Update score Board, with value of user to be displayed
7. Reset Score button will reset the value to `0` for current user (default_user)
8. Reset Board will remove all the user and reinitialise the value with default_user with score 0

### API OUTPUT

#### From UI:
1. By clicking the link `API Get Rank and Score` , a new tab with json result of rank and score from  API is displayed
2. By clicking the link `API Get n Player` , a new tab with json result of top n rank player  with score from  API is displayed, along with current user

#### Curl

1. Score and Rank API

```
curl --location --request GET 'localhost:5000/rank'
	 
       output:

	{
   "member": "default_user",
   "rank": 9,
   "score": 90.0,
   "top_score": 116.0,
   "top_user": "user_twtkq845",
   "total_users": 31
}
```
2. Top `n` Rank Player

```

		Top 5 user:

	curl --location --request GET 'http://localhost:5000/scoreboard?number=5'
	
	output:
	
	[
   {
       "member": "user_twtkq845",
       "rank": 1,
       "score": 116.0
   },
   {
       "member": "user_yngjl582",
       "rank": 2,
       "score": 112.0
   },
   {
       "member": "user_wjxwi967",
       "rank": 3,
       "score": 111.0
   },
   {
       "member": "user_ernuq231",
       "rank": 4,
       "score": 111.0
   },
   {
       "member": "user_jiuvp950",
       "rank": 5,
       "score": 108.0
   },
   {
       "member": "default_user",
       "rank": 9,
       "score": 90.0
   }
]
```
